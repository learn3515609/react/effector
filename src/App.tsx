import React from 'react';
import { MyForm } from './MyForm';

function App() {
  return <>
    <h1>Test Effector & formik</h1>
    <MyForm />
  </>;
}

export default App;
