import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { changeForm, fetchForm, form } from './store';
import { useStore, useEvent } from 'effector-react';
 
 const SignupSchema = Yup.object().shape({
   name: Yup.string()
     .min(2, 'Too Short!')
     .max(10, 'Too Long!')
     .required('Required'),
   age: Yup.number()
     .min(10, 'Too young!')
     .max(80, 'Too adult!')
     .required('Required'),
 });



export const MyForm = () => {
  const loadData = useEvent(fetchForm);
  const loading = useStore(fetchForm.pending);
  const value = useStore(form);

  return <Formik
    initialValues={value}
    onSubmit={async (values) => {
      changeForm(values);
    }}
    validationSchema={SignupSchema}
    enableReinitialize
  >
    {({ errors, touched }) => (
      <Form>
        <div>
          <label htmlFor="firstName">Name </label>
          <Field id="name" name="name" placeholder="enter name" style={{borderColor: errors.name ? 'red' : ''}} />
          {errors.name ? (
             <div>{errors.name}</div>
           ) : null}
        </div>

        <div>
          <label htmlFor="firstName">Age </label>
          <Field type="range" id="age" name="age" min={0} max={100} />
          {value.age}
          {errors.age ? (
             <div>{errors.age}</div>
           ) : null}
        </div>
        {loading ? 'loading' : <>
          <button type="submit">Submit</button>
          <button type="button" onClick={loadData}>load data</button>
        </>}
      </Form>

    )}
  </Formik>;
}