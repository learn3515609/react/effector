import { fetchForm, form, initialFormState } from "./store";

const changeForm = require('./store').changeForm;

describe('test effector store', () => {
  beforeEach(() => {
    changeForm(initialFormState)
  })
  test('test event', () => {
    changeForm({name: 'test', age: 30})
    const state = form.getState();
    expect(state).toEqual({name: 'test', age: 30})
  })

  test('test effect', async () => {
    await fetchForm();
    const state = form.getState();
    expect(state).toEqual({name: 'server',age: 75})
  })
})

export {}