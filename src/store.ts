import { createEvent, createStore, createEffect } from 'effector';
import { sleep } from './utils';

type TUser = {
  name: string,
  age: number
}

export const initialFormState = {
  name: '',
  age: 55,
}

const fetchUsers = async (): Promise<TUser> => {
  await sleep(2000);
  return {
    name: 'server',
    age: 75
  }
}

export const changeForm = createEvent<TUser>();

export const fetchForm = createEffect(fetchUsers);

export const form = createStore<TUser>(initialFormState)

form
  .on(changeForm, (state, form) => ({ ...state, ...form }))
  .on(fetchForm.doneData, (state, form) => ({ ...state, ...form }));

//form.watch(console.log)